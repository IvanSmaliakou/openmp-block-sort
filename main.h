#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>

#define MAX_RAND 2147483647
#define THREAD_TO_BUCKET_RATIO 100

void swap(long *arr, unsigned long long i, unsigned long long j)
{
    long buf = arr[i];
    arr[i] = arr[j];
    arr[j] = buf;
    return;
}

long *copy_arr(long *arr, unsigned long long size){
    long *copy = malloc(sizeof(long) * size);
    for (unsigned long long i = 0; i < size; i++){
        copy[i] = arr[i];
    }
    return copy;
}

// in my implementaiton pivot is the arr[0]
void quick_sort(long *arr, unsigned long long from_index, unsigned long long to_index)
{
    if (from_index >= to_index || to_index > MAX_RAND)
    {
        return;
    }
    long pivot = arr[from_index];
    unsigned long long i = from_index + 1;
    unsigned long long j = to_index;
    while (i <= j)
    {
        while (j > 0 && j >= i && arr[j] > pivot)
        {
            j--;
        }
        while (i < ULLONG_MAX && i <= j && arr[i] <= pivot)
        {
            i++;
        }
        if (i <= j && j > 0)
        {
            swap(arr, i, j);
            i++;
            j--;
        }
    }
    swap(arr, from_index, j);
    if (j > 1)
    {
        quick_sort(arr, from_index, j-1);
    }
    if (i < to_index)
    {
        quick_sort(arr, i, to_index);
    }
    return;
}

void block_sort(long *arr, unsigned long long size)
{
    int num_threads;
#pragma omp parallel
    {
        num_threads = omp_get_num_threads();
    }
    int num_buckets = num_threads * THREAD_TO_BUCKET_RATIO;
    int block_size = MAX_RAND / num_buckets + 1;                          // could be slightly unevenly distributed
    unsigned long long expected_max_bucket_size = size / num_buckets * 3; // suppose we do not expect the "fattest" bucket would have more than 3 * sizeof(average bucket)
    unsigned long long *bucket_lens = calloc(num_buckets, sizeof(unsigned long long));
    long **buckets = malloc(sizeof(long *) * num_buckets);
    for (int i = 0; i < num_buckets; i++)
    {
        buckets[i] = malloc(sizeof(long) * expected_max_bucket_size);
    }
#pragma omp parallel
    {
        int num_threads_2 = omp_get_num_threads();
        printf("\nnum of threads: %d\n", num_threads_2);
#pragma omp for schedule(static, size / num_threads)
        for (unsigned long long i = 0; i < size; i++)
        {
            int bucket_number = arr[i] / block_size;
            if (bucket_lens[bucket_number] >= expected_max_bucket_size)
            {
                perror("WARNING: exceeded bucket size");
            }
            unsigned long long write_index;
            #pragma omp atomic capture
            write_index = bucket_lens[bucket_number]++;
            buckets[bucket_number][write_index] = arr[i];
        }
        printf("buckets contents: ");
        for (int i = 0; i < num_buckets; i++)
        {
            printf("bucket %d (len=%d): ", i, bucket_lens[i]);
            fflush(stdout);
            for (int j = 0; j < bucket_lens[i]; j++)
            {
                printf("%ld ", buckets[i][j]);
                fflush(stdout);
            }
        }
        fflush(stdout);
        int current_thread_num = omp_get_thread_num();
        printf("quick sort: bucket number %d, thread number: %d", current_thread_num, current_thread_num);
        fflush(stdout);
        #pragma omp for schedule(static, num_buckets/num_threads)
        for (int i = 0; i < num_buckets; i++){
            quick_sort(buckets[i], 0, bucket_lens[i] - 1);
        }
    }
    printf("buckets contents after sort: ");
    for (int i = 0; i < num_buckets; i++)
    {
        printf("bucket %d: ", i);
        fflush(stdout);
        for (int j = 0; j < bucket_lens[i]; j++)
        {
            printf("%ld ", buckets[i][j]);
            fflush(stdout);
        }
    }
    fflush(stdout);
    unsigned long long c = 0;
    for (int i = 0; i < num_buckets; i++)
    {
        for (int j = 0; j < bucket_lens[i]; j++)
        {
            arr[c] = buckets[i][j];
            c++;
        }
    }
    return;
}

long *generate_array(unsigned long long size)
{
    long *array = malloc(sizeof(long) * size);
    unsigned short x16v[3];
#pragma omp parallel private(x16v)
    {
        int num_threads = omp_get_num_threads();
        printf("number of threads: %d \n", num_threads);
        x16v[1] = omp_get_thread_num();
        printf("thread number: %d\n", x16v[1]);

#pragma omp for schedule(dynamic)
        for (unsigned long long i = 0; i < size; i++)
        {
            x16v[0] = (short)((long)((long double)omp_get_wtime() * (long double)1e6) % (long)SHRT_MAX);
            x16v[2] = (short)((long)((long double)omp_get_wtime() * (long double)1e6) % (long)SHRT_MAX);
            array[i] = nrand48(x16v);
        }
    }
    return array;
}

unsigned long long str_to_ulong(char *str)
{
    int mult = 1;
    int e_pos = -1;
    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i] == 'e')
        {
            e_pos = i;
            break;
        }
    }
    unsigned long long mantiss = 0;
    unsigned long long exponent = 0;
    if (e_pos == -1)
    {

        for (int i = strlen(str) - 1; i >= 0; i--)
        {
            int digit = str[i] - 48;
            mantiss += digit * mult;
            mult *= 10;
        }
        return mantiss;
    }
    for (int i = e_pos - 1; i >= 0; i--)
    {
        int digit = str[i] - 48;
        mantiss += digit * mult;
        mult *= 10;
    }

    mult = 1;
    for (int i = strlen(str) - 1; i > e_pos; i--)
    {
        int digit = str[i] - 48;
        exponent += digit * mult;
        mult *= 10;
    }
    return mantiss * pow((double)10, (double)exponent);
}

void print_array(long *array, unsigned long long size)
{
    for (unsigned long long i = 0; i < size; i++)
    {
        printf("%ld ", array[i]);
    }
    printf("\n");
}

int cmp_asc(const void *elem1, const void *elem2){
    long el1 = *(long*)(elem1);
    long el2 = *(long*)(elem2);
    if (el1 > el2){
        return 1;
    }
    if (el2 > el1){
        return -1;
    }
    return 0;
}

void test_sorting(long *raw, long *sorted, unsigned long long size)
{
    qsort(raw, size, sizeof(long), cmp_asc);
    // assertion
    for (unsigned long long i = 0; i < size; i++){
        if (raw[i] != sorted[i]){
            printf("raw[i] != sorted[i]; i=%llu, raw[i]=%ld, sorted[i]=%d \n", i, raw[i], sorted[i]);
        }
    }
}