#include "main.h"
#include "errno.h"

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        perror("ERROR: usage: ./prog x y z, where x is the size of the array, y is a bucket size (both using either \"1e2\" or \"100\" notation) \n, z is a filename for csv file");
        return -1;
    }
    unsigned long long array_size = str_to_ulong(argv[1]);
    // unsigned long long bucket_size = str_to_ulong(argv[2]);
    // char *filename = argv[3];
    long *arr = generate_array(array_size);
    long *test_arr = copy_arr(arr, array_size);
    printf("array before: ");
    print_array(arr, 20);
    block_sort(arr, array_size);
    printf("array after: ");
    print_array(arr, 20);
    test_sorting(test_arr, arr, array_size);
    return 0;
}